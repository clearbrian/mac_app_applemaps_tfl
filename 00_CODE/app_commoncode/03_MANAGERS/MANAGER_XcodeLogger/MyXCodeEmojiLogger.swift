//
//  MyXCodeEmojiLogger.swift
//  joyride
//
//  Created by Brian Clear on 28/09/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

open class MyXCodeEmojiLogger {
    
    // use colored Emojis for better visual distinction
    // of log level for Xcode 8
//    let levelPrefix_verbose   = "💜 [VRBSE] "
    
    //---------------------------------------------------------------------
    let icon_debug     = "💛"
    let icon_info      = "💛"
    let icon_warning   = "💜"
    let icon_error     = "💔"

    //---------------------------------------------------------------------
    var levelPrefix_debug     = ""
    var levelPrefix_info      = ""
    var levelPrefix_warning   = ""
    var levelPrefix_error     = ""

    //---------------------------------------------------------------------

    init(){
        levelPrefix_debug     = "\(icon_debug) [DEBUG] "
        levelPrefix_info      = "\(icon_info) [INFO ] "
        levelPrefix_warning   = "\(icon_warning) [WARN ] "
        levelPrefix_error     = "\(icon_error) [ERROR] "
        //highlight is below
    }
    
    
    //At end
    let levelSuffix = ""
    //---------------------------------------------------------------------
    // \r was cuasing problems

    let levelPrefix_highlight = "🎾 [HIGH 0] ==========================================================================================================================="
    let levelSuffix_highlight = "🌕 [HIGH 1] ==========================================================================================================================="
    
    
    class var defaultInstance : MyXCodeEmojiLogger {
        struct Singleton {
            static var instance = MyXCodeEmojiLogger()
        }
        return Singleton.instance
    }
    

    
    //------------------------------------------------
    //CONFIGURE THE LOGGER
    //------------------------------------------------
    //APP WIDE DEFAULT
    //COMMENT ONLY 1 of these in to turn logging output levels up or down
    //    public var outputLogLevel: LogLevel = .None_0           /*  NO OUTPUT - secure if no println() in your code - use only if errors/crashes logged somewhere e.g. Flurry */
    //.Highlight - viewWillAppear
    //    public var outputLogLevel: MyXCodeLogLevel = .Error_1          /*  RELEASE - shows errors in console so you can see live app by plugging it in and opening Devices */
    //    public var outputLogLevel: MyXCodeLogLevel = .Warning_3        /* not used */
    //    public var outputLogLevel: MyXCodeLogLevel = .Info_4           /*  DEFAULT - self.log.info() for general info viewWillAppear etc */
    open var outputLogLevel: MyXCodeLogLevel = .debug_5          /*  DEFAULT - dev use self.log.debug() for dev classes may subclass configureLogging() and turn down to .Info_3 */
    //    public var outputLogLevel: MyXCodeLogLevel = .Highlight_6      /*  SPECIAL CASE Highlight always shown */
    
    //--------------------------------------------------------------
    // MARK: - PUBLIC
    // MARK: -
    //--------------------------------------------------------------


    open func debug(_ message: Any, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line){
//        self.log(message, levelPrefix: levelPrefix_debug, levelSuffix:"" , functionName: functionName, fileName: fileName, lineNumber: lineNumber)
    }
    
    open func info(_ message: Any, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line){
//        self.log(message, levelPrefix: levelPrefix_info, levelSuffix:"" , functionName: functionName, fileName: fileName, lineNumber: lineNumber)
    }
    
    open func warning(_ message: Any, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line){
//        self.log(message, levelPrefix: levelPrefix_warning, levelSuffix:"" , functionName: functionName, fileName: fileName, lineNumber: lineNumber)
    }
    
    open func error(_ message: Any, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line){
        self.log(message, levelPrefix: levelPrefix_error, levelSuffix:"" , functionName: functionName, fileName: fileName, lineNumber: lineNumber)
    }
    
    open func highlight(_ message: Any, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line){
//        self.log(message, levelPrefix: levelPrefix_highlight, levelSuffix: levelSuffix_highlight, functionName: functionName, fileName: fileName, lineNumber: lineNumber)
        
        let fileFunctionsInfoStr = self.fileNameLineNumberAndSelector(functionName: functionName, fileName: fileName, lineNumber: lineNumber)
//        print("\(levelPrefix_highlight)")
//        print("\(message) \r............ \(fileFunctionsInfoStr)")
//        print("\(levelSuffix_highlight)")


    }
    
    //---------------------------------------------------------------------
    func log(_ message: Any, levelPrefix: String, levelSuffix: String,
             functionName: String = #function,
             fileName: String = #file,
             lineNumber: Int = #line
        )
    {
        
        let fileFunctionsInfoStr = self.fileNameLineNumberAndSelector(functionName: functionName, fileName: fileName, lineNumber: lineNumber)
        //print("\r\(levelPrefix) \(message) \(levelSuffix)\r\(fileFunctionsInfoStr)")
        print("\r\(levelPrefix) \(fileFunctionsInfoStr) \(message) \(levelSuffix)")
        
    }
    //OUtputs
    //[COLCPlacePickerViewController.swift:961 callWS_NearbySearch(_:radius:)]
    func fileNameLineNumberAndSelector(
        functionName: String = #function,
        fileName: String = #file,
        lineNumber: Int = #line) -> String
    {
        //---------------------------------------------------------------------
        let fileNameNSString = fileName as NSString
        let fileFunctionsInfoString = "[" + fileNameNSString.lastPathComponent + ":" + String(lineNumber) + " " + functionName + "]"
        return fileFunctionsInfoString
        //---------------------------------------------------------------------
    }
}


//--------------------------------------------------------------
// MARK: -
// MARK: - LogLevel
//--------------------------------------------------------------
public enum MyXCodeLogLevel : Int {
    case none_0 = 0   /* USE IF YOU DONT WANT ANY OUTPUT - prob wont see error in console - errors should be logged somewhere Flurry/WS */
    case highlight_1  /* SPECIAL CASE */
    case error_2      /* TURN TO THIS FOR RELEASE */
    case warning_3
    case info_4       /* turn down to this when you dont want to see ALL debug code for class */
    case debug_5      /* self.log.debug() when youre developing code */
    
    
    public func description() -> String {
        switch self {
        case .debug_5:
            return "Debug_5"
        case .info_4:
            return "Info__4"
        case .warning_3:
            return "Warn__3"
        case .error_2:
            return "Error_2"
        case .highlight_1:
            return "Highl_1"
        case .none_0:
            return "None__0"
        }
    }
    //------------------------------------------------
}

