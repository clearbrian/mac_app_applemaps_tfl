//
//  ViewController.swift
//  mac_app_applemaps_tfl
//
//  Created by Brian Clear on 10/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Cocoa
import CoreLocation
import MapKit

class ViewController: NSViewController, CurrentLocationManagerDelegate {

    @IBOutlet weak var mkMapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //appDelegate.currentLocationManager.c
        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    func currentLocationUpdated(currentLocationManager: CurrentLocationManager, currentLocation: CLLocation)
    {
        
        //let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        let region = MKCoordinateRegion(center: currentLocation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
        
        self.mkMapView.setRegion(region, animated: true)
        
        
        
        
        let mkPointAnnotation = MKPointAnnotation()
        
        //mkPointAnnotation.coordinate = CLLocationCoordinate2D(latitude: 11.12, longitude: 12.11)
        
        mkPointAnnotation.coordinate = currentLocation.coordinate
        
        mkMapView.addAnnotation(mkPointAnnotation)
        
    }
    func currentLocationFailed(currentLocationManager: CurrentLocationManager, error: Error?)
    {
            print("ERROR:currentLocationFailed >> ViewController")
    }


}

