//
//  AppDelegate.swift
//  mac_app_applemaps_tfl
//
//  Created by Brian Clear on 10/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Cocoa
import CoreLocation
//so we can reference it from anywhere

//------------------------------------------------------------------------------------------------
//App Delegate - MacOS
//------------------------------------------------------------------------------------------------
let sharedApplication = NSApplication.shared()
//let sharedApplication = UIApplication.shared
//------------------------------------------------------------------------------------------------
//App Delegate - iOS
//------------------------------------------------------------------------------------------------
let appDelegate = sharedApplication.delegate as! AppDelegate


//if #available(iOS 9, OSX 10.10, *) {
//    // Use iOS 9 APIs on iOS, and use OS X v10.10 APIs on OS X
//} else {
//    // Fall back to earlier iOS and OS X APIs
//}




@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate, CurrentLocationManagerDelegate {

    //--------------------------------------------------------------------------------------------------------------------------
    //LOGGING - Requires XCodeColors plugin
    //--------------------------------------------------------------------------------------------------------------------------
    //MUST BE DECLARED IN EACH CLASS THAT USES IT
    //let log = MyXCodeColorsLogger.defaultInstance
    //-------------------------------------------------------------------
    //create seperate instance for each VC so we can turn them on/off
    //as we develop them in configureLogging()
    //let log = MySwiftyBeaverLogger()
    //let log = MyXCodeEmojiLogger.defaultInstance
    //--------------------------------------------------------------------------------------------------------------------------
    let log = MyXCodeEmojiLogger.defaultInstance
    //--------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------------------------

    // TODO: - MAC - move this
    var googleSupportedLanguageCodeForDeviceLanguage: String?
    

    //------------------------------------------------------------------------------------------------
    //GOOGLE
    //------------------------------------------------------------------------------------------------
    //
    //https://console.developers.google.com/
    //USE FIREFOX...
    //brian.clear@cityoflondonconsulting.com   << USE FIREFOX... MAKE SURE YOURE LOGGED IN AS THIS!! NOT clearbrian@googlemail.com
    //N5!
    //https://docs.google.com/document/d/18SYp2cI-NTpjA-mK3j6USMS4vkzvCyHKoeBWz3314Yc/edit
    
    
    //    //------------------------------------------------------------------------------------------------
    //    //iOS key 1
    //    //------------------------------------------------------------------------------------------------
    //
    //    //https://console.developers.google.com/apis/credentials/key/171?project=tfl-taxi-ranks
    //used by Google SDK - geocode etc
    let googleMapsApiKey_iOS = "AIzaSyDSY9eXMUphG9oc-qfkgKDDmFrGH7N4Yl4"
    //
    //    //------------------------------------------------------------------------------------------------
    //    //BrowserKey 1
    //    //------------------------------------------------------------------------------------------------
    //    //Requires Referrer -
    //    //*.cityoflondonconsulting.com/*
    //    // let googleMapsApiKey_Web = "AIzaSyCh_0tfBGDU7l_RHXuq5zol9amft7e3szg"
    //    let googleMapsApiKey_Web =  "AIzaSyD-UQTJ4W1RgtJhamGO7fobLg1at7X1jss"
    
    //no restriction for Directions - web call
    let googleMapsApiKey_Web =  "AIzaSyBRBz99dZS2LnD3h6hFJ5_pOCiWD2JB-1o"
    
    
    //
    //    //------------------------------------------------------------------------------------------------
    //    //SERVER KEY 1
    //    //------------------------------------------------------------------------------------------------
    //    //lets you add in IP but is also optional
    //    //used for Directions API
    //    let googleMapsApiKey_Serverkey = "AIzaSyDEZ4bn5vsRhbEawKAv274_EU74nfINHDE"
    
    
    //warning unrestricted - couldnt get iOS to w
    //    let googleMapsApiKey1 = "AIzaSyBRBz99dZS2LnD3h6hFJ5_pOCiWD2JB-1o"
    
    //NOTE: if you add an API and its having problems then go to Docs site on google for that API and press GET A KEY
    // when ai added Directions I cliked on GET A KEY and it set up SERVER KEY not iOS key
    //https://developers.google.com/maps/documentation/directions/
    //---------------------------------------------------------------------
    //JAN 27 I changed it from a http website key to ios key - same string
    
    
    
    //--------------------------------------------------------------------
    //GEOCODERs
    //-------------------------------------------------------
    //var colcGeocoder = COLCGeocoder()
    //--------------------------------------------------------------------

    
    
    //---------------------------------------------------------------------
    // MARK: -
    // MARK: - CurrentLocationManager
    //---------------------------------------------------------------------
    
    //---------------------------------------------------------------------
    //LOCATION  - CurrentLocationManager
    //---------------------------------------------------------------------
    var currentLocationManager = CurrentLocationManager()
    //currentLocationManager.delegate = self done in application:didFinish
    //also called .requestLocation
    
    
    
    
    //--------------------------------------------------------------
    // MARK: - CurrentLocationManagerDelegate
    // MARK: -
    //--------------------------------------------------------------
    func currentLocationUpdated(currentLocationManager: CurrentLocationManager, currentLocation: CLLocation){
        
        //---------------------------------------------------------------------
        //forward to TOP VC
        //---------------------------------------------------------------------
        //does nothing
        //        if let colcPlacePickerViewController = self.colcPlacePickerViewController{
        //            // TODO: - rename to currentLocationUpdated
        //            colcPlacePickerViewController.moveToCurrentLocation()
        //        }else{
        //            //self.log.error("colcPlacePickerViewController is nil - set in COLCPlacePickerViewController.viewDidLoad()")
        //        }
        //        //---------------------------------------------------------------------
        //        if let tripPlannerViewController = self.tripPlannerViewController{
        //            tripPlannerViewController.currentLocationUpdated(currentLocation)
        //        }else{
        //            self.log.error("appD.tripPlannerViewController is nil - set in COLCTripPlannerViewController.viewDidLoad()")
        //        }
        //---------------------------------------------------------------------
        //NOISY
        log.error("TODO APPD currentLocationUpdated:\(currentLocation)")
        
//        if let window = self.window {
//            if let rootUINavigationController = window.rootViewController as? UINavigationController{
//                if let topViewController = rootUINavigationController.topViewController {
//                    if topViewController is CurrentLocationManagerDelegate{
//                        if let topViewControllerCurrentLocationManagerDelegate = topViewController as? CurrentLocationManagerDelegate {
//                            topViewControllerCurrentLocationManagerDelegate.currentLocationUpdated(currentLocationManager: currentLocationManager,
//                                                                                                   currentLocation: currentLocation)
//                        }else{
//                            appDelegate.log.error("topViewController as? CurrentLocationManagerDelegate  is nil")
//                        }
//                    }else{
//                        //noisyappDelegate.log.error("topViewController:[\(topViewController)] doesnt implement CurrentLocationManagerDelegate - cant forward current location")
//                    }
//                }else{
//                    appDelegate.log.error("rootUINavigationController.topViewController is nil")
//                }
//            }else if let rootUITabBarController = window.rootViewController as? UITabBarController{
//                if let selectedViewController = rootUITabBarController.selectedViewController {
//                    
//                    //UITabBarController >> UINavigationController
//                    if let rootUINavigationController = selectedViewController as? UINavigationController{
//                        
//                        //UITabBarController >> UINavigationController >> VC<CurrentLocationManagerDelegate>
//                        if let topViewController = rootUINavigationController.topViewController {
//                            if topViewController is CurrentLocationManagerDelegate{
//                                if let topViewControllerCurrentLocationManagerDelegate = topViewController as? CurrentLocationManagerDelegate {
//                                    topViewControllerCurrentLocationManagerDelegate.currentLocationUpdated(currentLocationManager: currentLocationManager,
//                                                                                                           currentLocation: currentLocation)
//                                }else{
//                                    appDelegate.log.error("topViewController as? CurrentLocationManagerDelegate  is nil")
//                                }
//                            }else{
//                                //noisy
//                                //appDelegate.log.error("topViewController:[\(topViewController)] doesnt implement CurrentLocationManagerDelegate - cant forward current location")
//                            }
//                        }else{
//                            appDelegate.log.error("rootUINavigationController.topViewController is nil")
//                        }
//                    }else{
//                        appDelegate.log.error("window.rootViewController as? UINavigationController is nil:window.rootViewController \(window.rootViewController)")
//                    }
//                    
//                    
//                }else{
//                    appDelegate.log.error("rootUINavigationController.topViewController is nil")
//                }
//            }else{
//                appDelegate.log.error("window.rootViewController as? UINavigationController is nil:window.rootViewController \(window.rootViewController)")
//            }
//        }else{
//            appDelegate.log.error("self.window is nil")
//        }
        //-------------------------------------------------------------------
        
//        if let mainWindow = sharedApplication.mainWindow {
//            //-------------------------------------------------------------------
//            if let contentViewController = mainWindow.contentViewController {
//                if contentViewController is CurrentLocationManagerDelegate{
//                    if let currentLocationManagerDelegate = contentViewController as? CurrentLocationManagerDelegate {
//                        
//                        currentLocationManagerDelegate.currentLocationUpdated(currentLocationManager: currentLocationManager,
//                                                                                     currentLocation: currentLocation)
//                        
//                    }else{
//                        appDelegate.log.error("topViewController as? CurrentLocationManagerDelegate  is nil")
//                    }
//                }else{
//                    //noisy
//                    appDelegate.log.error("topViewController:[\(contentViewController)] doesnt implement CurrentLocationManagerDelegate - cant forward current location")
//                }
//            }
//            //-------------------------------------------------------------------
//            
//        }else{
//            appDelegate.log.error("sharedApplication.mainWindow is nil - did you call")
//        }
        //-------------------------------------------------------------------
        sharedApplication.sendLocationToTopController(currentLocationManager: currentLocationManager, currentLocation: currentLocation)
    }
    
    func currentLocationFailed(currentLocationManager: CurrentLocationManager, error: Error?)
    {
        self.log.error("currentLocationFailed: error:\(error) - MAP MAY BE STUCK")
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - APP START
    //--------------------------------------------------------------
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        
        
        //------------------------------------------------------------------------------------------------
        // Dont do window operations here - sharedApplication.mainWindow stll nil - use applicationDidBecomeActive
        //also dont do requestLocation as the loc on mac is fixed so sent immediately but wond not ready
        
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    public func applicationDidBecomeActive(_ notification: Notification) {
        //do all windows ops here - sharedApplication.mainWindow should be not nil
        
        //------------------------------------------------------------------------------------------------
        //START LOCATION - quicker on mac as usually set once unless laptop
        //------------------------------------------------------------------------------------------------
        
        if let _ = sharedApplication.mainWindow {
            //-------------------------------------------------------------------
            self.currentLocationManager.delegate = self
            self.currentLocationManager.requestLocation()
            //delegate will forward it to window.contentViewController so make sure ts ready
            //-------------------------------------------------------------------
            
        }else{
            appDelegate.log.error("[applicationDidBecomeActive] sharedApplication.mainWindow is nil - did you call requestLoc too early before window was ready")
        }
    }
}

